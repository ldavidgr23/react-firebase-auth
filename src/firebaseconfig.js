import firebase from 'firebase'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyCWaPCUqebY1J753YVo0vcr1Hi4TujUIB4",
    authDomain: "react1-24297.firebaseapp.com",
    projectId: "react1-24297",
    storageBucket: "react1-24297.appspot.com",
    messagingSenderId: "473289945973",
    appId: "1:473289945973:web:adf650d0ac362c8d89d93c",
    measurementId: "G-Z7JSCM4VPL"
  };
  // Initialize Firebase
  const fire= firebase.initializeApp(firebaseConfig);
  const auth = fire.auth()
  export {auth}
  firebase.analytics();